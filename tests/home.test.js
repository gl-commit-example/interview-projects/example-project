describe('Home page', () => {
  beforeAll(async () => {
    await page.goto('http://localhost:8080');
  });

  test('should render correct <title>', async () => {
    await expect(page.title()).resolves.toMatch('Hello World');
  });

  test('should say "Hello World"', async () => {
    const h1 = await page.evaluate(() => document.body.textContent);
    expect(h1).toContain('Hello World');
  });
});
